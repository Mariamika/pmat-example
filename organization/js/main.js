var start = new Date(),
    prevDay,
    startHours = 0;

start.setHours(00);
start.setMinutes(0);
start.setSeconds(0);

$(function () {

    $('#timepicker-fine-date').datepicker({
        timepicker: true,
        startDate: start,
        minHours: startHours,
        showSecond: true,
        maxHours: 24,
        format: 'd/m/Y hh:mm:ss',
        onSelect: function (fd, d, picker) {
            // Ничего не делаем если выделение было снято
            if (!d) return;

            var day = d.getDay();

            // Обновляем состояние календаря только если была изменена дата
            if (prevDay != undefined && prevDay == day) return;
            prevDay = day;
        }
    });

    $('.js-select2-multiple').select2();

    $('.js-timepicker').datepicker({
        timepicker: false,
        // startDate: start,
        // minHours: startHours,
        maxHours: 24
    });

    $('.js-select2').select2();

    $('.js-checkbox-all-items-in-table').on('change', function () {

        var checkboxClassName = $(this).data('checkbox-class');
        if ($(this).is(':checked')) {
            $('.' + checkboxClassName).attr('checked', 'checked');
        } else {
            $('.' + checkboxClassName).removeAttr('checked');
        }

    });

    $('[data-inputmask]').inputmask();

    // Зададим стартовую дату
    var start = new Date(),
        finish = new Date(),
        prevDay,
        startHours = 0;

    start.setHours(00);
    start.setMinutes(0);

    finish.setHours(00);
    finish.setMinutes(0);

    $('#timepicker-start').datepicker({
        timepicker: true,
        startDate: start,
        minHours: startHours,
        maxHours: 24,
        onSelect: function (fd, d, picker) {
            // Ничего не делаем если выделение было снято
            if (!d) return;

            var day = d.getDay();

            // Обновляем состояние календаря только если была изменена дата
            if (prevDay != undefined && prevDay == day) return;
            prevDay = day;
        }
    });

    $('#timepicker-end').datepicker({
        timepicker: true,
        startDate: finish,
        minHours: startHours,
        maxHours: 24,
        onSelect: function (fd, d, picker) {
            // Ничего не делаем если выделение было снято
            if (!d) return;

            var day = d.getDay();

            // Обновляем состояние календаря только если была изменена дата
            if (prevDay != undefined && prevDay == day) return;
            prevDay = day;
        }
    });

    $('#download-organization-report').click(function () {
        window.location = '/report/report?' + http_build_query({
            from: $('#timepicker-start').val(),
            to: $('#timepicker-end').val()
        });

        return false;
    });

    /**
     * Подготавливаем GET параметры
     *
     * @param formdata
     * @param numeric_prefix
     * @param arg_separator
     * @returns {string}
     */
    function http_build_query(formdata, numeric_prefix, arg_separator) {
        var key, use_val, use_key, i = 0, tmp_arr = [];

        if (!arg_separator) {
            arg_separator = '&';
        }

        for (key in formdata) {
            use_key = escape(key);
            use_val = escape((formdata[key].toString()));
            use_val = use_val.replace(/%20/g, '+');

            if (numeric_prefix && !isNaN(key)) {
                use_key = numeric_prefix + i;
            }
            tmp_arr[i] = use_key + '=' + use_val;
            i++;
        }

        return tmp_arr.join(arg_separator);
    }
});
