$(function () {

    $('.js-logout-button').on('click', function(){
        $.ajax({
            url: '/logout',
            type: 'POST',
            success: function() {
                window.location = 'https://parkomatica.ru';
            }
        });
    });

    $('.js-button-widget').on('click', function () {
        var widget = new cp.CloudPayments();

        var publicId = $(this).data('publicId');
        var accountId = $(this).data('accountId');
        var invoiceId = $(this).data('invoiceId');
        var description = $(this).data('description');
        var amount = $(this).data('amount');

        widget.charge({ // options
                publicId: publicId,
                accountId: accountId,
                invoiceId: invoiceId,
                description: description,
                amount: amount,
                currency: 'RUB'
            },
            function (options) { // success
                console.log(options);
                window.location = '/invoices?card=1';
//                $('#checkout-result').text('Оплата завершена');
            },
            function (reason, options) { // fail
                alert('Привязка карты неудалась! Попробуйте снова или обратитесь в службу поддержки');
            });
    });

    $('#js-button-widget').on('click', function () {
        var widget = new cp.CloudPayments();

        var publicId = $(this).data('publicId');
        var accountId = $(this).data('accountId');
        var invoiceId = $(this).data('invoiceId');
        var description = $(this).data('description');
        var amount = $(this).data('amount');

        widget.charge({ // options
                publicId: publicId,
                accountId: accountId,
                invoiceId: invoiceId,
                description: description,
                amount: amount,
                currency: 'RUB'
            },
            function (options) { // success
                console.log(options);
                window.location = '/invoices?card=1';
//                $('#checkout-result').text('Оплата завершена');
            },
            function (reason, options) { // fail
                alert('Привязка карты неудалась! Попробуйте снова или обратитесь в службу поддержки');
            });
    });

    // Обновление страницы активных сессий
    if ($('.active-sessions-block').length || $('.active-organization-sessions-block').length) {
        var url = '';
        if ($('.active-sessions-block').length) {
            url = '/control/session/active-sessions-ajax';
        } else {
            url = '/organization/parking/active-sessions-ajax';
        }

        setInterval(function() {
            $.ajax({
                url: url,
                type: 'GET',
                success: function(data) {
                    $('.content-block').html(data);
                }
            });
        }, 10000);
    }

    // Обновление страницы сессий должников
    if ($('.debt-sessions-block').length) {
        setInterval(function() {
            $.ajax({
                url: '/control/session/debt-ajax',
                type: 'GET',
                success: function(data) {
                    $('.content-block').html(data);
                }
            });
        }, 10000);
    }
});